/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * File Name          : freertos.c
  * Description        : Code for freertos applications
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

	#include "i2c.h"
	#include "mpu.h"
	#include "MadgwickAHRS.h"
	#include "lcd.h"
	#include "3Dbox.h"
	#include "usart.h"
	#include "ringbuffer_dma.h"

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN Variables */

/* USER CODE END Variables */
osThreadId readSensorsHandle;
osThreadId drawBoxHandle;
osThreadId waitCommandHandle;

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN FunctionPrototypes */

	osMailQId mailYPRHandle;
	osMailQId mailCMDHandle;

	/* Data structure for MPU9250 angles */
	typedef struct {
		float yaw, pitch, roll;
	} YPR;

	/* Data structure for bluetooth commands */
	typedef struct {
		char command[16];
		uint16_t arg[3];
	} CMD;

/* USER CODE END FunctionPrototypes */

void StartReadSensors(void const * argument);
void StartDrawBox(void const * argument);
void StartWaitCommand(void const * argument);

void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/* GetIdleTaskMemory prototype (linked to static allocation support) */
void vApplicationGetIdleTaskMemory( StaticTask_t **ppxIdleTaskTCBBuffer, StackType_t **ppxIdleTaskStackBuffer, uint32_t *pulIdleTaskStackSize );

/* USER CODE BEGIN GET_IDLE_TASK_MEMORY */
static StaticTask_t xIdleTaskTCBBuffer;
static StackType_t xIdleStack[configMINIMAL_STACK_SIZE];

void vApplicationGetIdleTaskMemory( StaticTask_t **ppxIdleTaskTCBBuffer, StackType_t **ppxIdleTaskStackBuffer, uint32_t *pulIdleTaskStackSize )
{
  *ppxIdleTaskTCBBuffer = &xIdleTaskTCBBuffer;
  *ppxIdleTaskStackBuffer = &xIdleStack[0];
  *pulIdleTaskStackSize = configMINIMAL_STACK_SIZE;
  /* place for user code */
}
/* USER CODE END GET_IDLE_TASK_MEMORY */

/**
  * @brief  FreeRTOS initialization
  * @param  None
  * @retval None
  */
void MX_FREERTOS_Init(void) {
  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* definition and creation of readSensors */
  osThreadDef(readSensors, StartReadSensors, osPriorityHigh, 0, 512);
  readSensorsHandle = osThreadCreate(osThread(readSensors), NULL);

  /* definition and creation of drawBox */
  osThreadDef(drawBox, StartDrawBox, osPriorityLow, 0, 512);
  drawBoxHandle = osThreadCreate(osThread(drawBox), NULL);

  /* definition and creation of waitCommand */
  osThreadDef(waitCommand, StartWaitCommand, osPriorityNormal, 0, 512);
  waitCommandHandle = osThreadCreate(osThread(waitCommand), NULL);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */

	/* Create queues */
	/* definition and creation of mailYPR */
	osMailQDef(mailYPR, 16, YPR);
	mailYPRHandle = osMailCreate(osMailQ(mailYPR), NULL);

	/* definition and creation of mailCMD */
	osMailQDef(mailCMD, 16, CMD);
	mailCMDHandle = osMailCreate(osMailQ(mailCMD), NULL);

  /* USER CODE END RTOS_THREADS */

}

/* USER CODE BEGIN Header_StartReadSensors */
/**
  * @brief  Function implementing the readSensors thread.
  * @param  argument: Not used
  * @retval None
  */
/* USER CODE END Header_StartReadSensors */
void StartReadSensors(void const * argument)
{
  /* USER CODE BEGIN StartReadSensors */
	/* Mail queue variables */
		YPR* data;
		/* MPU9250 variables */
		/* Variables for all measurements */
		int16_t acc[3] = { };
		int16_t gyro[3] = { };
		int16_t mag[3] = { };
		uint16_t accScale = 0;
		float gyroScale = 0.f;
		float magScale = 32760.0f / 4912.0f;
		float ax, ay, az;
		float gx, gy, gz;
		float mx, my, mz;
		/* Timestamp & status variables */
		int16_t intStatus;
		uint32_t lastTime, currentTime;
		/* Start MPU9250 and change settings */
		mpu_i2c_init(&hi2c1);
		mpu_init(NULL);
		mpu_set_sensors(INV_XYZ_GYRO | INV_XYZ_ACCEL | INV_XYZ_COMPASS);
		mpu_set_sample_rate(500);
		mpu_get_accel_sens(&accScale);
		mpu_get_gyro_sens(&gyroScale);

		/* Init Madgwick filter */
		Madgwick_init();
		lastTime = HAL_GetTick();
		uint32_t index = 0;
  /* Infinite loop */
  for(;;)
  {
	/* Get current mpu chip status */
		mpu_get_int_status(&intStatus);
		if (intStatus & MPU_INT_STATUS_DATA_READY) {
			/* Get all raw measurements */
			mpu_get_accel_reg(acc, 0);
			mpu_get_gyro_reg(gyro, 0);
			mpu_get_compass_reg(mag, 0);
			/* Convert to real units */
			ax = (float) (acc[0]) / accScale;
			ay = (float) (acc[1]) / accScale;
			az = (float) (acc[2]) / accScale;
			gx = gyro[0] / gyroScale;
			gy = gyro[1] / gyroScale;
			gz = gyro[2] / gyroScale;
			mx = mag[1] / magScale;
			my = mag[0] / magScale;
			mz = - mag[2] / magScale;
			/* Do data processing by Madgwick filter */
			currentTime = HAL_GetTick();
			Madgwick_update(gx, gy, gz, ax, ay, az, mx, my, mz, (currentTime - lastTime) / 1000.0);
			lastTime = currentTime;
		}
		/* Send data every 4th calculation */
		if (index++ % 4 == 0) {
			/* Allocate new cell in mail queue */
			data = (YPR *) osMailAlloc(mailYPRHandle, 5);
			if (data != NULL) {
				/* Fill in data */
				data->pitch = Madgwick_getPitchRadians();
				data->roll = Madgwick_getRollRadians();
				data->yaw = Madgwick_getYawRadians();
				/* Put the mail in the queue */
				osMailPut(mailYPRHandle, data);
			}
		}
		osDelay(10);
		//osDelay(1);
  }
  /* USER CODE END StartReadSensors */
}

/* USER CODE BEGIN Header_StartDrawBox */
/**
* @brief Function implementing the drawBox thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartDrawBox */
void StartDrawBox(void const * argument)
{
  /* USER CODE BEGIN StartDrawBox */
	/* Mail queue variables */
		YPR* data;
		osEvent evt;
		/* 3Dbox variables */
		Object3d__HandleTypeDef box;
		uint16_t boxWidth = 120, boxDepth = 80, boxHeight = 10;
		/* 3Dbox initialization */
		LCD_Init();
		Object3d_InitBox(&box, boxWidth, boxDepth, boxHeight);
  /* Infinite loop */
  for(;;)
  {
	/* Get new mail from the queue */
	evt = osMailGet(mailYPRHandle, 0);
	if (evt.status == osEventMail) {
		/* Obtain data from mail */
		data = (YPR *) evt.value.p;
		/* Redraw 3Dbox*/
		Object3d_CleanObject(&box);
		Object3d_SetRotation(&box, data->pitch, data->roll, data->yaw);
		Object3d_DrawObject(&box);
		/* Free the cell in mail queue */
		osMailFree(mailYPRHandle, data);
	}
	osDelay(1);
    //osDelay(1);
  }
  /* USER CODE END StartDrawBox */
}

/* USER CODE BEGIN Header_StartWaitCommand */
/**
* @brief Function implementing the waitCommand thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartWaitCommand */
void StartWaitCommand(void const * argument)
{
  /* USER CODE BEGIN StartWaitCommand */
  /* Infinite loop */
  for(;;)
  {
    osDelay(1);
  }
  /* USER CODE END StartWaitCommand */
}

/* Private application code --------------------------------------------------*/
/* USER CODE BEGIN Application */

/* USER CODE END Application */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
